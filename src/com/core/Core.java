package com.core;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;

import com.core.server.Server;
import com.core.tick.Ticker;
import com.core.tick.Timings;
import com.game.event.EventManager;
import com.lib.Log;

/**
 * Represents the core, responsible for running the server. Should keep this
 * open to allow for running of multiple servers.
 * 
 * This class contains a static thread pool, shared across the JVM.
 * 
 * @author netherfoam
 *
 */
public class Core {
	/**
	 * Handles scheduling of tasks for a later date, optionally on the main
	 * thread or on an async thread
	 */
	private static Scheduler scheduler;

	/**
	 * A thread pool for handling async tasks that are not on the main server
	 * thread.
	 */
	private static ExecutorService threadPool;

	/**
	 * The server that is currently running.
	 */
	private static Server server;

	/**
	 * The timings for tracking lag and expensive operations down
	 */
	private static Timings timings;

	private static EventManager eventManager;
	private static Ticker ticker;

	/**
	 * This class loader is used so that the Module system works. It is a shared
	 * ClassLoader that allows the Module system to load classes, independant of
	 * where the class is stored. For example, AIModule should be able to access
	 * a class from MusicModule using this ClassLoader.
	 */
	public static final ClassLoader CLASS_LOADER = ClassLoader.getSystemClassLoader();

	/**
	 * Initializes the core of the server
	 * 
	 * @param threads
	 *            the max number of threads to run with, or <= 0 to use the
	 *            number of processors available to the runtime as the number of
	 *            threads.
	 * @throws Exception
	 *             If there was an error binding the port or loading the cache.
	 */
	public static void init(int threads, String[] args) throws Exception {
		getTimings(); // Force load timings

		Log.info("-- Server Booting at " + new Date().toString() + " --");

		if (threads <= 0)
			threads = Runtime.getRuntime().availableProcessors();
		Log.info("Booting. Core threads: " + threads);
		final long start = System.currentTimeMillis();

		threadPool = Executors.newFixedThreadPool(threads, new ThreadFactory() {
			private int nextThreadId = 0;

			@Override
			public Thread newThread(Runnable r) {
				Thread t = new Thread(r, "ExecutorService " + nextThreadId++);
				t.setContextClassLoader(CLASS_LOADER);
				t.setPriority(Thread.MIN_PRIORITY);
				t.setDaemon(true);
				return t;
			}
		});

		server = new Server("localhost", 34848);
		scheduler = new Scheduler(server.getServerThread(), threadPool);
		server.getServerThread().submit(getTicker());
		server.start();
		submit(server, true);

		// Hint to the garbage collector it should run now.
		System.gc();

		getServer().getServerThread().submit(new Runnable() {
			@Override
			public void run() {
				Log.info("Booted in " + (System.currentTimeMillis() - start) + "ms.");
			}
		});
	}

	/**
	 * Fetches the file that contains org.maxgamer.rs.Core, whether it be a JAR
	 * file or a compiled BIN folder containing .class files.
	 * 
	 * @return the location of the main program.
	 */
	public static File getCodeSource() {
		try {
			URL url = Core.class.getProtectionDomain().getCodeSource().getLocation();
			return new File(url.toURI().getPath());

		} catch (URISyntaxException e) {
			throw new RuntimeException("Couldn't locate Code Source. Source "
					+ Core.class.getProtectionDomain().getCodeSource().getLocation().toString());
		}
	}

	/**
	 * Submits a Runnable task for execution and returns a Future representing
	 * that task. The Future's get method will return null upon successful
	 * completion. The task is scheduled as soon as possible by the thread pool,
	 * and may not be done in sync.
	 * 
	 * @param r
	 *            The runnable task to execute
	 */
	public synchronized static Future<?> submit(Runnable r, boolean async) {
		if (async)
			return threadPool.submit(r);
		else {
			return getServer().getServerThread().submit(r);
		}
	}

	/**
	 * Submits the given task for execution after the given number of
	 * milliseconds delay. The task is guaranteed to wait at least delay
	 * milliseconds, but is not guaranteed to be executed if the task list is
	 * saturated.
	 * 
	 * @param r
	 *            The runnable
	 * @param delay
	 *            The task delay in milliseconds.
	 */
	public synchronized static Future<Void> submit(Runnable r, long delay, boolean async) {
		return scheduler.queue(r, delay, async);
	}

	/**
	 * Retrieves the current server that is running.
	 * 
	 * @return the server
	 */
	public static Server getServer() {
		return server;
	}

	public synchronized static Timings getTimings() {
		if (timings == null) {
			timings = new Timings();
		}
		return timings;
	}

	private Core() {
		// Private Constructor
	}

	public static EventManager getEvents() {
		if (eventManager == null)
			eventManager = new EventManager();
		return eventManager;
	}

	public static Ticker getTicker() {
		if (ticker == null)
			ticker = new Ticker();
		return ticker;
	}
}
