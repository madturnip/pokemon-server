package com.core.server;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

import com.game.network.ChannelHolderSet;
import com.game.network.GameChannel;
import com.game.network.message.IncomingGameMessage;

/**
 * @author Albert Beaupre
 */
public class Server implements Runnable {

	public static final short MAXIMUM_CONNECTIONS = 10000;

	/**
	 * The socket address that this server will read/write to and from
	 */
	private final InetSocketAddress address;
	private final ServerThread serverThread;
	private final ChannelHolderSet<GameChannel> connections;

	public Server(String ip, int port) {
		address = new InetSocketAddress(ip, port);
		this.serverThread = new ServerThread(this);
		this.connections = new ChannelHolderSet<>(MAXIMUM_CONNECTIONS);
	}

	@Override
	public void run() {
		try {
			ServerSocketChannel server = ServerSocketChannel.open();
			server.configureBlocking(false);
			server.socket().bind(address);
			Selector selector = Selector.open();
			server.register(selector, SelectionKey.OP_ACCEPT);

			for (;;) {
				selector.select();
				Iterator<SelectionKey> i = selector.selectedKeys().iterator();
				while (i.hasNext()) {
					SelectionKey key = (SelectionKey) i.next();
					i.remove();
					if (key.isAcceptable()) {
						SocketChannel client = server.accept();
						System.out.println("Client connected: " + client.getRemoteAddress());
						client.configureBlocking(false);
						client.register(selector, SelectionKey.OP_READ);
						connections.registerChannel(new GameChannel(client));
						continue;
					}
					if (key.isReadable()) {
						SocketChannel client = (SocketChannel) key.channel();
						ByteBuffer buffer = ByteBuffer.allocate(32);
						try {
							int numRead = client.read(buffer);
							if (numRead > 0) {
								byte[] data = new byte[numRead];
								System.arraycopy(buffer.array(), 0, data, 0, numRead);
								connections.get(client).readMessage(new IncomingGameMessage(data));
							}
						} catch (Exception e) {
							e.printStackTrace();
							continue;
						}
						continue;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void start() {
		//serverThread.submit(this);
		serverThread.start();
	}

	public ServerThread getServerThread() {
		return serverThread;
	}
}
