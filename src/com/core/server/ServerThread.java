package com.core.server;

import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;

import co.paralleluniverse.fibers.FiberExecutorScheduler;
import co.paralleluniverse.fibers.FiberScheduler;

import com.core.Core;
import com.lib.Calc;

/**
 * @author netherfoam
 */
public class ServerThread implements Executor {
	private ArrayList<Runnable> queue;
	private Server server;
	private long start;

	private Thread thread;

	private long working = 0;
	private FiberScheduler fex;

	public ServerThread(Server server) {
		this.server = server;
		this.queue = new ArrayList<Runnable>();
		this.fex = new FiberExecutorScheduler("ServerThread-FiberExec", this);
	}

	public Server getServer() {
		return this.server;
	}

	/**
	 * Fetches the amount of time that the ServerThread has been active as a
	 * decimal. Eg, 1.0 = used 100% of the time, and 0.20 means used 20% of the
	 * time. You may reset this (To sample over a certain time) via resetUsage()
	 * 
	 * @return the amount of time the ServerThread has been active
	 */
	public double getUsage() {
		long now = System.currentTimeMillis();
		long time = now - this.start;
		return Calc.betweend(0, this.working / (double) time, 1);
	}

	/**
	 * Returns true if the current thread is the server thread
	 * 
	 * @return true if the current thread is the server thread
	 */
	public boolean isServerThread() {
		if (this.thread == null) {
			return false;
		}
		if (Thread.currentThread().getId() == this.thread.getId()) {
			return true;
		}
		return false;
	}

	/**
	 * Resets the current usage / running / uptime information for the
	 * ServerThread.
	 */
	public void resetUsage() {
		this.working = 0;
		this.start = System.currentTimeMillis();
	}

	/**
	 * Joins with the server thread until it stops
	 */
	public void shutdown() {
		Thread end = this.thread;
		this.thread = null;
		try {
			end.join();
		} catch (InterruptedException e) {
			// What can you do?
			e.printStackTrace();
		}
	}

	/**
	 * Starts the server thread running.
	 * 
	 * @throws IllegalStateException
	 *             if the server thread is running
	 */
	public void start() {
		if (this.thread != null) {
			throw new IllegalStateException("ServerThread already started.");
		}
		this.start = System.currentTimeMillis();
		this.thread = new Thread("ServerThread-thread") {
			@Override
			public void run() {
				long time;
				while (ServerThread.this.thread != null) {
					ArrayList<Runnable> tasks;
					synchronized (ServerThread.this.queue) {
						if (ServerThread.this.queue.isEmpty()) {
							try {
								ServerThread.this.queue.wait();
							} catch (InterruptedException e) {
							}
						}
						assert ServerThread.this.queue.isEmpty() == false;

						time = System.currentTimeMillis();
						tasks = new ArrayList<Runnable>(ServerThread.this.queue);
						ServerThread.this.queue.clear();
					}

					// for(Runnable r : tasks){
					for (int i = 0; i < tasks.size(); i++) {
						Runnable r = tasks.get(i);
						try {
							// Log.debug("Running task " + r);
							r.run();
						} catch (Throwable t) {
							t.printStackTrace(System.out);
						}

						synchronized (r) {
							// Notifies any future's waiting on get() that R is
							// completed.
							r.notifyAll();
						}
					}
					ServerThread.this.working += (System.currentTimeMillis() - time);
				}
			}
		};
		this.thread.setContextClassLoader(Core.CLASS_LOADER);
		this.thread.setPriority(Thread.MAX_PRIORITY);
		this.thread.start();
	}

	public Future<Void> submit(Runnable r) {
		ServerThreadTask t = new ServerThreadTask(r, false);
		synchronized (this.queue) {
			this.queue.add(r);
			this.queue.notify();
		}
		return t;
	}

	public FiberScheduler getFiberScheduler() {
		return fex;
	}

	@Override
	public void execute(Runnable command) {
		this.submit(command);
	}

	/**
	 * Asserts that the current thread is the ServerThread / Primary thread.
	 * 
	 * @throws IllegalThreadException
	 *             if the assertion fails
	 */
	public void assertThread() {
		if (Thread.currentThread() != this.thread) {
			throw new IllegalArgumentException("Attempted to run thread " + Thread.currentThread() + " but must be only run on the ServerThread "
					+ this.thread);
		}
	}
}