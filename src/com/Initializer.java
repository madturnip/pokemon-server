package com;

import com.core.Core;

public class Initializer {

	public static void main(String[] args) throws Exception {
		Core.init(Runtime.getRuntime().availableProcessors() - 1, args);
	}
}
