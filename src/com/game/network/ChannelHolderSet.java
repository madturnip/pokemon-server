package com.game.network;

import java.io.IOException;
import java.nio.channels.SocketChannel;

public class ChannelHolderSet<S extends ChannelHolder> {

	private final S[] holders;

	@SuppressWarnings("unchecked")
	public ChannelHolderSet(int capacity) {
		this.holders = (S[]) new ChannelHolder[capacity];
	}

	public boolean registerChannel(S holder) {
		if (!holder.getChannel().isConnected())
			throw new IllegalArgumentException("A channel holder added to the list must be connected.");
		int indexOf = indexOf(holder.getChannel());
		if (indexOf != -1) {
			holders[indexOf] = holder;
			return false;
		}
		int index = freeIndex();
		if (index == -1)
			throw new IndexOutOfBoundsException("The list has reached its maximum amount of holders.");
		holders[index] = holder;
		return true;
	}

	public boolean unregisterChannel(S holder) {
		int indexOf = indexOf(holder.getChannel());
		if (indexOf != -1) {
			holders[indexOf] = null;
			try {
				holder.getChannel().close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}

	public boolean contains(SocketChannel channel) {
		for (S s : holders)
			if (s != null && s.getChannel().equals(channel))
				return true;
		return false;
	}

	public S get(SocketChannel channel) {
		for (S s : holders)
			if (s != null && s.getChannel().equals(channel))
				return s;
		return null;
	}

	private int freeIndex() {
		for (int i = 0; i < holders.length; i++)
			if (holders[i] == null)
				return i;
		return -1;
	}

	private int indexOf(SocketChannel s) {
		for (int index = 0; index < holders.length; index++) {
			if (holders[index] == null)
				continue;
			if (holders[index].getChannel().equals(s))
				return index;
		}
		return -1;
	}

	public int size() {
		int count = 0;
		for (int i = 0; i < holders.length; i++)
			if (holders[i] != null)
				count++;
		return count;
	}

}
