package com.game.network.message;

public class IncomingGameMessage implements IncomingMessage {

	private final byte operationCode;
	private byte[] data;
	private int readIndex;

	public IncomingGameMessage(byte[] data) {
		this.data = data;
		this.operationCode = data[readIndex++];
	}

	private byte readByte() {
		return data[readIndex++];
	}

	public String readString() {
		StringBuilder builder = new StringBuilder();
		byte b;
		while ((b = readByte()) != 0)
			builder.append((char) b);
		return builder.toString();
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> T read(Class<? extends T> cast) {
		if (cast == String.class)
			return (T) readString();
		return null;
	}

	@Override
	public byte operationCode() {
		return operationCode;
	}

	@Override
	public byte[] data() {
		return data;
	}

}
