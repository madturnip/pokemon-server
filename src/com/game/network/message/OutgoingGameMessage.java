package com.game.network.message;

public class OutgoingGameMessage implements OutgoingMessage {
	private final byte operationCode;

	private byte[] data;
	private byte writeIndex;

	public OutgoingGameMessage(int operationCode, int length) {
		this.data = new byte[length + 1];
		this.operationCode = (byte) operationCode;
		data[writeIndex++] = (byte) operationCode;
	}

	public OutgoingGameMessage writeByte(byte b) {
		data[writeIndex++] = b;
		return this;
	}

	public OutgoingGameMessage writeBytes(byte... bytes) {
		for (byte b : bytes)
			writeByte(b);
		return this;
	}

	public OutgoingGameMessage writeInt(int i) {
		if (writeIndex >= data.length)
			throw new IllegalArgumentException("Writer index is already at its limit.");

		writeByte((byte) ((i >> 24) & 0xFF));
		writeByte((byte) ((i >> 16) & 0xFF));
		writeByte((byte) ((i >> 8) & 0xFF));
		writeByte((byte) (i & 0xFF));
		return this;
	}

	@Override
	public <T> OutgoingMessage write(T type, Class<? extends T> cast) {
		if (cast == Integer.class || cast == int.class)
			return writeInt((Integer) type);
		return this;
	}

	@Override
	public byte operationCode() {
		return operationCode;
	}

	@Override
	public byte[] data() {
		return data;
	}

}
