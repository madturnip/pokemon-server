package com.game.network.message;

/**
 * @author Albert Beaupre
 */
public interface IncomingMessage {

	/**
	 * Reads the data from this message.
	 * 
	 * @param cast
	 *            the cast of the type
	 * @return the type read
	 */
	<T> T read(Class<? extends T> cast);

	/**
	 * Returns the operation code of this message for identification.
	 * 
	 * @return the operation code
	 */
	byte operationCode();

	/**
	 * Returns an array of bytes that is encoded or decoded based on the message
	 * type.
	 * 
	 * @return the array of bytes for this message
	 */
	byte[] data();
}
