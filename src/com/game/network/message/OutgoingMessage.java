package com.game.network.message;

/**
 * @author Albert Beaupre
 */
public interface OutgoingMessage {

	/**
	 * Writes the specified {@code type} to this {@code OutgoingMessage}.
	 * 
	 * @param type
	 *            the type to write
	 * @param cast
	 *            the cast of the type
	 * @return a new instance of the message with the written type
	 */
	<T> OutgoingMessage write(T type, Class<? extends T> cast);

	/**
	 * Returns the operation code of this message for identification.
	 * 
	 * @return the operation code
	 */
	byte operationCode();

	/**
	 * Returns an array of bytes that is encoded or decoded based on the message
	 * type.
	 * 
	 * @return the array of bytes for this message
	 */
	byte[] data();

}
