package com.game.network;

import java.nio.channels.SocketChannel;

public interface ChannelHolder {

	SocketChannel getChannel();

}
