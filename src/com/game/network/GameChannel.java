package com.game.network;

import java.nio.channels.SocketChannel;

import com.game.network.message.IncomingMessage;
import com.game.network.message.OutgoingMessage;

public class GameChannel implements ChannelHolder {

	private final SocketChannel channel;

	public GameChannel(SocketChannel channel) {
		this.channel = channel;
	}

	@Override
	public SocketChannel getChannel() {
		return channel;
	}

	public void writeMessage(OutgoingMessage message) {

	}

	public void readMessage(IncomingMessage message) {
		System.out.println("READING");
	}

}
