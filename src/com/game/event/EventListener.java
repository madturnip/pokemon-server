package com.game.event;

import com.core.Core;

/**
 * @author Albert Beaupre
 */
public interface EventListener {

	/**
	 * Registers this {@code EventListener} to the {@code EventManager} inside
	 * {@code Core}.
	 */
	default void register() {
		Core.getEvents().register(this);
	}

	/**
	 * Unregisters this {@code EventListener} from the {@code EventManager}
	 * inside {@code Core}.
	 */
	default void unregister() {
		Core.getEvents().unregister(this);
	}

}
