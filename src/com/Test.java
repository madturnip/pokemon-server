package com;

import com.core.Core;
import com.core.tick.Tickable;
import com.game.event.Event;
import com.game.event.EventHandler;
import com.game.event.EventListener;

public class Test {

	public static void main(String[] args) throws Exception {
		Core.init(Runtime.getRuntime().availableProcessors() - 1, args);

		EventListener listener = new EventListener() {
			@EventHandler() //This must be added to any method that you want to listen for events
			public void whatever(Event event) { // this method can have any name
				/**
				 * This method can have any name but wont be called unless the
				 * only parameter is an instant of Event.
				 * 
				 * If an Event that is called is an instance of the parameter,
				 * then this method will be executed.
				 * 
				 * So, if I have a MapUpdateEvent and my parameter is
				 * whatever(MapUpdateEvent event), then whenever MapUpdateEvent
				 * is called anywhere...this method is executed.
				 */
				new Tickable() {
					final int count = (int) (Math.random() * 3);
					int c = 0;
					@Override
					public void tick() {
						if (c++ >= count) {
							cancel();
							return;
						}
						queue(1000);
					}
				}.queue(1000); //queues this tickable in 1000 milliseconds (1 second)
			}
		};
		listener.register(); // Registers this listener so it can listen for events

		Event event = new Event(); // Extend an Event class to create different instances if you want to isolate different events
		event.call();
	}

}
